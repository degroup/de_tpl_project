# {package_name} {project_type} {package_version}

{package_name} is a Python multi-platform {project_type} project based on the [__Kivy__ Framework](https://kivy.org) 
and some portions of the [__ae__ namespace(Application Environment)](https://ae.readthedocs.io "ae on rtd").

the source code is available at [Gitlab]({repo_url}) maintained by the user group {repo_group}.

additional credits to:

* [__Erokia__](https://freesound.org/people/Erokia/) and 
  [__plasterbrain__](https://freesound.org/people/plasterbrain/) at
  [freesound.org](https://freesound.org) for the sounds.
* [__iconmonstr__](https://iconmonstr.com/interface/) for the icon images.
