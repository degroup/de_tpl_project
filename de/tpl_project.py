"""
outsourced Python project files templates
=========================================

this package provides generic templates of basic project files for all types of Python projects. this includes project
configuration files like e.g. `.gitignore`, as well as generic project documentation files like `CONTRIBUTING.rst` or
`LICENSE.md`.
"""

__version__ = '0.2.3'
